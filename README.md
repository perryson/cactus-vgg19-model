# Cactus VGG19 Model

I trained a Pytorch VGG-19 model to detect columnar cactuses in images. As of now, it has an accuracy rate of 98.95%. The convolutional neural network utilized the loss function Cross Entropy and optimizer Adamax. 



#  Steps to display the Jupyter Notebook 
1. Select the file CancerDetection.ipynb. 
2. Select Default File Viewer.
3. Select Ipython Notebook.

# Data
[Data source](https://www.kaggle.com/c/aerial-cactus-identification/data)